require 'add_two_numbers'
require 'test/unit'

class TestAdding < Test::Unit::TestCase
  def test_adding
    [0, ,1].each do |n1|
      [0, 1, 9, 99, 999].each do |n2|
        sum = n1 + n2
        n1_numberic = Numberic.from_i(n1)
        n2_numberic = Numberic.from_i(n2)
        assert_equal(sum, n1_numberic.add(n2_numberic).to_i)
      end
    end
  end
end
