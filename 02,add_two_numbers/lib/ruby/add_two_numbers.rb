#
class ListNode
  attr_accessor :val, :next

  def self.from_i(int)
    maker(int.digits)
  end

  def self.maker(digits)
    if digits.empty?
      nil
    else
      new_node = new(digits.first)
      new_node.next = maker(digits.drop(1))
      new_node
    end
  end

  def initialize(val)
    @next = nil
    @val = val
  end

  def add(summand)
    adder(self, summand)
  end

  def to_i
    _to_i(self, 0)
  end

  private

  def adder(n1, n2, carry = 0, memo = nil)
    if n1.nil? && n2.nil?
      return(carry == 1 ? ListNode.new(1) : nil)
    elsif n1.nil?
      summand1 = ListNode.new(0)
      summand2 = n2
    elsif n2.nil?
      summand1 = n1
      summand2 = ListNode.new(0)
    else
      summand1 = n1
      summand2 = n2
    end

    provisional = summand1.val + summand2.val + carry

    if provisional > 9
      sum = provisional - 10
      overflow = 1
    else
      sum = provisional
      overflow = 0
    end

    result_digit = ListNode.new(sum)
    result_digit.next = adder(
      summand1.next,
      summand2.next,
      overflow
    )
    result_digit
  end

  def _to_i(list, exp)
    if list.nil?
      0
    else
      list.val * 10**exp + _to_i(list.next, exp + 1)
    end
  end
end

# @param {ListNode} l1
# @param {ListNode} l2
# @return {ListNode}
def add_two_numbers(n1, n2)
  n1.add(n2)
end
