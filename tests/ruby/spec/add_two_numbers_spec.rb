require 'spec_helper'
require 'add_two_numbers_helper'
require 'add_two_numbers'

describe ListNode do
  [0, 1, 948, 96114].each do |first|
    [0, 1, 948, 96114].each do |second|
      sum = first + second
      it "#{first} + #{second} = #{sum}" do
        expect(add_two_numbers(
                 ListNode.from_i(first),
                 ListNode.from_i(second)
               ).to_i
              ).to eq(sum)
      end
    end
  end
end
