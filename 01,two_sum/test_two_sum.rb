require 'minitest/autorun'
require_relative 'two_sum'

class TestTwoSum < MiniTest::Unit::TestCase
  def setup
    @two_sum = TwoSum.new
    @summands = (0..9).to_a
  end

  def test_two_sum_8
    assert_equal(
      [0,8],
      @two_sum.two_sum(@summands, 8)
    )
  end
end
