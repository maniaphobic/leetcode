# Given an array of integers, return indices of the two numbers such
# that they add up to a specific target.
#
# You may assume that each input would have exactly one solution, and
# you may not use the same element twice.

class TwoSum
  def to_hash(summands)
    index = -1
    Hash[
      summands.map do |summand|
        index += 1
        [summand, index]
      end
    ]
  end

  #
  # @param {Integer[]} nums
  # @param {Integer} target
  # @return {Integer[]}
  def two_sum(nums, target)
    index = -1
    nums_hash = Hash[
      nums.map do |summand|
        index += 1
        [summand, index]
      end
    ]
    (0..(nums.length - 1)).to_a.each do |first_index|
      first_summand = nums[first_index]
      second_summand = target - first_summand
      second_index = nums_hash.fetch(second_summand, -1)
      if second_index >= 0 && first_index != second_index
         return([first_index, second_index])
      end
    end
    []
  end
end
