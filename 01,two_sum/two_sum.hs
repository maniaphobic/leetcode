import qualified Data.Map.Strict as Map

_toMap [] _ = Map.empty
_toMap (key:rest) index =
  Map.insert key index (_toMap rest (index + 1))

toMap list = _toMap list 0

query key map = Map.lookup key map

nums = [1..3]

letts = ["a", "b", "c"]

(map (\lett -> lett) letts)
