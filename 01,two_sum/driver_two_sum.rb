#!/usr/bin/env ruby

require_relative 'two_sum'

pp TwoSum.new.two_sum((0..9).to_a, 8)

#[4,9,7,2,3]
